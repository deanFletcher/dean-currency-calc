import pytest

from currency_converter.currency_calc import call_currency, parse_currency_resp, currency_exchange_rate, \
    get_all_symbols, validate_currency


@pytest.mark.parametrize('curr1, curr2', [('GBP', 'EUR')])
def test_call_currency_api_success(curr1, curr2):
    response = call_currency(curr1, curr2)
    assert "rates" in response


def test_parse_currency_response():
    mock_response = {'rates': {'GBP': '0.7911049873931029', 'EUR': '0.9227622098116341'}}
    currency_list = parse_currency_resp(mock_response, 'GBP')
    assert "GBP" in currency_list[0]
    assert "EUR" in currency_list[1]


def test_currency_exchange_rate():
    exchange_rate, selling, buying = currency_exchange_rate(
        [{'EUR': '0.9227622098116341'}, {'GBP': '0.7911049873931029'}])
    assert 1.17 == exchange_rate
    assert 'EUR' == selling
    assert 'GBP' == buying


def test_get_all_symbols():
    list_symbols = get_all_symbols()
    assert 'EUR' in list_symbols
    assert 'USD' in list_symbols


def test_validate_currency_symbol():
    currency_list = {'USD': "US Dollar"}
    symbol_found = validate_currency('USD', currency_list)
    assert symbol_found is True
