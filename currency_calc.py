import decimal
import json

import requests

api_key = '27948cbfe0bf497a94ab66e836c18385'

currency_url = 'https://api.currencyfreaks.com/v2.0/rates/latest'

symbols_url = 'https://api.currencyfreaks.com/v2.0/currency-symbols'


def call_currency(curr1, curr2):
    symbols = f'{curr1},{curr2}'
    url_with_params = f'{currency_url}?apikey={api_key}&symbols={symbols}'
    response = requests.get(url_with_params)
    json_response = json.loads(response.content)
    return json_response


def parse_currency_resp(resp, curr1):
    currency_list = []
    for curr in resp['rates']:
        single_currency = resp['rates'][curr]
        if curr1 == curr:
            currency_list.insert(0, {curr: single_currency})
        else:
            currency_list.append({curr: single_currency})
    return currency_list


def currency_exchange_rate(currencies):
    curr_selling = next(iter(currencies[0]))
    curr_buying = next(iter(currencies[1]))
    curr_sell_value = float(currencies[0][curr_selling])
    curr_buy_value = float(currencies[1][curr_buying])
    print(curr_sell_value)
    print(curr_buy_value)
    exchange_rate = (curr_sell_value / curr_buy_value)
    if exchange_rate >= 1:
        return round(float(exchange_rate), 2), curr_selling, curr_buying
    else:
        decimal.getcontext().prec = 17
        decimal.getcontext().Emin = -999999999
        decimal.getcontext().Emax = 999999999
        decimal.getcontext().capitals = 1
        number = decimal.Decimal(exchange_rate)
        return number, curr_selling, curr_buying


# currency_exchange_rate([{'EUR': '0.9227622098116341'}, {'GBP': '0.7911049873931029'}])

def get_all_symbols():
    sym_response = requests.get(url=symbols_url)
    converted_symbols = json.loads(sym_response.content)
    return converted_symbols['currencySymbols']


def validate_currency(user_curr, currency_list):
    is_valid = False
    for key in currency_list:
        if key == user_curr:
            is_valid = True
            break
    return is_valid


def lets_convert():
    while True:
        print('Welcome to Converters-r-Us, where you can convert your favorite coins all day long!')
        available_symbols = get_all_symbols()
        # Begin User Inputs for Currency Exchange Rate
        while True:
            coin_to_sell = str(input('Which currency do you want to exchange? Write Official Abbreviation: ')).upper()
            if validate_currency(coin_to_sell, available_symbols) is not True:
                print(
                    '-Sorry, that currency Name or Abbreviation is not valid. \n-Look up the valid Name/Abbrebiation and try again.')
            else:
                break
        while True:
            coin_to_buy = str(
                input('Which Currency do you want to exchange for? Write the Official Abbreviation: ')).upper()
            if validate_currency(coin_to_buy, available_symbols) is not True:
                print(
                    '-Sorry, that currency Name or Abbreviation is not valid. \n-Look up the valid Name/Abbrebiation and try again.')
            else:
                break
        currency_api_response = call_currency(coin_to_sell, coin_to_buy)
        parsed_response = parse_currency_resp(currency_api_response, coin_to_sell)
        exchange_rate, curr_selling, curr_buying = currency_exchange_rate(parsed_response)
        print(
            f'The Exchange Rate for {curr_selling} to {curr_buying} is {exchange_rate:.15f} {curr_selling} equals 1 {curr_buying}')


if __name__ == "__main__":
    lets_convert()
